# PHP development environment


## gitlab docker registry
Register the [docker registry](https://gitlab.com/help/user/packages/container_registry/index).

```
docker login registry.gitlab.com
docker login -u <username> -p <access_token> registry.gitlab.com
```

You need to do this only once.

## Extra tools

* [Captain](https://github.com/jenssegers/captain/)

## Note about ci

* public repository in gitlab: ```docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY```
* private repository in gitlab: ```docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY```

## Example for docker compose

.env file:
```
MYSQL_VERSION=mariadb:10.5
MYSQL_DATA=./data
MYSQL_PASSWORD=root

SRC_WEBSITE=../../website
SRC_ADMIN=../../backoffice

COMPOSER_PATH=~/.config/composer
```

docker-compose.yml file:
```
version: "3.2"

services:
  # databases
  db_mysql:
    image: ${MYSQL_VERSION}
    volumes:
      - type: bind
        source: ${MYSQL_DATA}
        target: /var/lib/mysql
        volume:
          nocopy: true
      - type: bind
        source: ./components/mysql-conf.d
        target: /etc/mysql/conf.d
        volume:
          nocopy: true
    ports:
      - '20001:3306'
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_PASSWORD}

  db_redis:
    image: redis:6.0-alpine
    ports:
      - '20002:6379'

  # useless but mandatory tools
  tools_db:
    image: phpmyadmin/phpmyadmin
    ports:
      - '20021:80'
    depends_on:
      - db_mysql
    links:
      - db_mysql:db
    environment:
       - MYSQL_ROOT_PASSWORD=${MYSQL_PASSWORD}

  tools_redis:
    image: faktiva/php-redis-admin
    ports:
      - '20022:80'
    depends_on:
      - db_redis
    links:
      - db_redis:redis
    environment:
      - PHPREDMIN_DATABASE_REDIS_0_HOST=redis

  mailcatcher:
    image: mailhog/mailhog
    ports:
      - '20010:8025'

  # the dev stuffs
  website:
    image: registry.gitlab.com/grummfy/docker-dev/docker-dev-php-web-7.4
    ports:
      - '8001:80'
    depends_on:
      - db_mysql
    volumes:
      - ${SRC_WEBSITE}:/var/www/src:cached
      - ${COMPOSER_PATH}:/var/www/composer:cached
    environment:
      - PUBLIC_PATH=www
      - APP_URL=http://localhost:8001

  admin:
    image: registry.gitlab.com/grummfy/docker-dev/docker-dev-php-web-7.4
    ports:
      - '8002:80'
    depends_on:
      - db_redis
      - db_mysql
    volumes:
      - ${SRC_ADMIN}:/var/www/src:cached
      - ${COMPOSER_PATH}:/var/www/composer:cached
      # overwrite php config
      #- ./some-config.ini:/usr/local/etc/php/conf.d/99-config.ini
    links:
      - mailcatcher
      - website
    environment:
      - PUBLIC_PATH=public
      - DB_HOST=db_mysql
      - DB_DATABASE=mydatabase
      - DB_USERNAME=root
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - REDIS_HOST=db_redis
      - CACHE_DRIVER=redis
      - MAIL_DRIVER=smtp
      - MAIL_HOST=mailcatcher
      - MAIL_PORT=1025
      - MAIL_ENCRYPTION=
      - QUEUE_DRIVER=redis
      - APP_URL=http://localhost:8002
      - APPLICATION_ENV=local
```
